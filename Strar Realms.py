###############################   STAR REALMS : Version simplifie   ######################################

import random as rdm
import matplotlib.pyplot as plt

############## Initialisation du paquet de carte #################

cartes_0 = [[0,0,1,"","Scout"],[0,1,0,"","Viper"]]
paquet = [[1,1,2,"bleu","Federation Shuttle"],[1,1,2,"bleu","Federation Shuttle"]
          ,[2,3,2,"bleu","cutter"],[2,3,2,"bleu","cutter"],[3,6,2,"bleu","embassy yacht"]
          ,[3,6,2,"bleu","embassy yacht"],[4,4,3,"bleu","barter world"]
          ,[5,6,0,"bleu","defense center"],[6,9,0,"bleu","flagship"],[7,0,5,"bleu","central office"]
          ,[8,12,0,"bleu","command ship"],[5,7,0,"jaune","war world"],[3,2,1,"jaune","survey ship"]
          ,[3,2,1,"jaune","survey ship"],[4,3,2,"jaune","space station"],[6,6,0,"jaune","royal reboubt"]
          ,[1,3,0,"jaune","imperial fighter"],[1,3,0,"jaune","imperial fighter"]
          ,[8,11,0,"jaune","fleet HQ"],[7,9,0,"jaune","Dreadnaught"],[2,5,0,"jaune","corvette"]
          ,[2,5,0,"jaune","corvette"],[1,2,1,"rouge","trade bot"],[1,2,1,"rouge","trade bot"]
          ,[3,2,2,"rouge","supply bot"],[3,2,2,"rouge","supply bot"],[4,4,3,"rouge","Patrol Mech"]
          ,[6,8,0,"rouge","missile mech"],[2,4,0,"rouge","missile bot"],[2,4,0,"rouge","missile bot"]
          ,[5,6,0,"rouge","battle mech"],[8,8,2,"rouge","brain world"],[7,7,1,"rouge","machine base"]
          ,[4,6,0,"vert","Blob Destroyer"],[3,1,3,"vert","Blob Wheel"]
          ,[3,1,3,"vert","Blob Wheel"],[1,3,0,"vert","SpikePod"]
          ,[1,3,0,"vert","SpikePod"],[5,3,3,"vert","The Hive"]
          ,[7,8,0,"vert","Mothership"],[6,6,0,"green","Battle Blob"]
          ,[8,4,4,"vert","Blob World"],[2,0,3,"vert","Trade Pod"]
          ,[2,0,3,"vert","Trade Pod"]
          ]

nbe_prospecteur = 10
cartes_par_couleur = 11

# cartes doit contenir toutes les cartes pour pouvoir donner un identifiant à chaque cartes
for i in range(0,nbe_prospecteur+1):    # ajout des prospecteurs
    cartes_0.append([2,0,2,"","Prospecteur"])
for extension in paquet: # on place une copie de chaque carte du paquet dans cartes
    cartes_0.append(extension)
for i in range(len(cartes_0)):        # ajout d'un identifiant a chaque carte
    cartes_0[i] = [i] + cartes_0[i]

cartes = cartes_0[:]

############## Fonction de jeu ###############"

#Parametres a initialiser

cartes_A = paquet[:] # les cartes qui seront disponible à l'achat
reference_achat = [["scout",0,0]] # on initialise les reference avec le scout en position 0 ( c'est son identifiant)
reference_point = [[0]]
magasin = []
taille_magasin = 5
deck = [[],[]] # une case pour chaque joueur
defausse = [[],[]]
main = [[],[]]
ide_prospecteur = [2] # il faudra changer ça, c'est moche !
points = [0,0]
gain = []
res = [0,0] # permet de donner les résultats d'un grand nombre de parties

####### Les fontions Fill_ #######

def remplir_magasin(magasin,cartes_A): # permet de remplir le magasin, très pratique dans toute la suite
    while len(magasin) < taille_magasin and cartes_A != []:
        tirage = rdm.randint(0,len(cartes_A)-1) 
        carte_pioche = cartes_A[tirage]
        magasin.append(carte_pioche)
        cartes_A.pop(tirage)

def remplir_deck(deck):  # ne s'effectue qu'une fois au debut de la partie
    for k in range(2):
        for i in range(8):
            deck[k].append(cartes[0]) # ajout de 8 éclaireur
        for j in range(2):
            deck[k].append(cartes[1]) # ajout de 2 viper

def remplir_ref(reference_achat,reference_point): # on precise que les references ne possedent pas d'utilité pour le jeu en lui même
                                       # mais nous permet de detecter la "puissance" de chaque carte, ce qui nous sert dans la partie 2
    for i in range(1,len(cartes)):
        reference_achat.append([cartes[i][5],0,0]) # le nom, le gain, le nbe de fois ou joue
        reference_point.append([i-1])

###

remplir_magasin(magasin,cartes_A)
remplir_deck(deck)
remplir_ref(reference_achat,reference_point)

##### Une fonction affichage qui epure le format des cartes ######


def affichage(liste):
    res = []
    for i in range(len(liste)):
        res.append((liste[i][5],liste[i][1])) # le nom, le cout
    print("#####  ",res,"   ####")


##### Une fonction qui reconnait la couleur des cartes jouées dans le tour #####


def empreinte_couleur(ensemble): # a coder mieux si possible
    if ensemble == "":
        return -1
    elif ensemble == "bleu":
        return 0
    elif ensemble == "jaune":
        return 1
    elif ensemble == "rouge":
        return 2
    else: # ça sera vert
        return 3


########### Les fonctions Corps ###########


def pioche(n,joueur,deck,main,defausse): # la fonction pioche
    nbe = 0
    while len(deck[joueur]) >= 1 and nbe < n: # il reste des cartes a piocer et dans le deck
        tirage = rdm.randint(0,len(deck[joueur])-1)
        carte_pioche = deck[joueur][tirage]
        deck[joueur].pop(tirage)
        main[joueur].append(carte_pioche)
        nbe += 1
    if nbe < n: # plus de carte dans le deck
        if len(defausse[joueur]) == 0: # evite une possible boucle infini
            return ()
        for j in range(len(defausse[joueur])): # on transfert la defausse dans le deck
            deck[joueur].append(defausse[joueur][-1])
            defausse[joueur].pop()
        pioche(n-nbe,joueur,deck,main,defausse) # on pioche les cartes qu'il nous reste

###

def acheter(carte,joueur,cartes_A,defausse,magasin): # prend en paramétre le numéro de la carte à acheté
    achat = magasin.pop(carte)
    defausse[joueur].append(achat)
    remplir_magasin(magasin,cartes_A)


def achat_auto(credits_tour,joueur,cartes_A,defausse,magasin,ide_prospecteur):
    possible = [] # va chercher les cartes dans son budget
    for i in range(len(magasin)):
        [ide,cout,degats,credit,couleur,nom] = magasin[i][:6]
        if cout <= credits_tour:
            possible.append((i,cout))
    if credits_tour >= 2 and ide_prospecteur[0] < 2 + nbe_prospecteur: # les prospecteur sont gerer a part
        possible.append((-1,2))
    if possible == []: # on ne trouve rien
        return []
    tirage = rdm.randint(0,len(possible)-1) # le joueur achete une carte au hasard
    (j,cout_final) = possible[tirage]
    if j >= 0: # la carte n'est pas un prospecteur
        memoire = magasin[j] # on garde en memoire momentanement la carte acheter
        acheter(j,joueur,cartes_A,defausse,magasin)
        return achat_auto(credits_tour - cout_final,joueur,cartes_A,defausse,magasin,ide_prospecteur) + [memoire] # recursivement
    else:      # c'est un prospecteur
        defausse[joueur].append(cartes[ide_prospecteur[0]])
        memoire = ide_prospecteur[0]
        ide_prospecteur[0] += 1  # il reste moins de prospecteur dans la pile
        return achat_auto(credits_tour - 2,joueur,cartes_A,defausse,magasin,ide_prospecteur) + [cartes[memoire]]


####### Fonction de redistribution des points ###############
# Cette partie utilise les deux tableaux references ( achat et point )
# Le premier contient à la fois les points qu'a permis de faire gagner la carte, le nombre de fois ou la carte à été joué et les successeur de la carte
# Le second contient les prédecesseur de la carte

def redistribution(reference_achat,reference_point,ide,point): # cette fonction rempli les references
    for pred in reference_point[ide][1:]: # pred = les cartes qui ont permi l'achat de la carte
        ide_p,participation = pred[0],pred[1]
        if ide_p != 0 and participation > 0 and cartes[ide][1] != 0 : # on cherche à remettre des points au predecesseur
            reference_achat[ide_p][1] += point * (participation / cartes[ide][1]) 
            redistribution(reference_achat,reference_point,ide_p,point) # il ne faut pas oublier les pred. des pred. et ect ...

#############

def jouer_un_tour_0(joueur,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur):
    credits_tot = 0
    couleur_tour = [0,0,0,0] # au debut aucune couleur n'a été joué
    jouer = [] # la liste des cartes joué durant le tour
    while main[joueur] != []: # on joue les cartes une par une
        [ide,cout,degats,credit,couleur,nom] = main[joueur][-1][:6] # on tronque une partie de la carte qui ne sera utile que plus tard
        # on stoque les identifiant pour plus tard
        jouer.append((ide,credit))
        reference_achat[ide][2] += 1 # la carte est joué une fois de plus 
        # gestion des points brut
        points_carte = degats
        # gestion des points d'allies
        allies = empreinte_couleur(couleur)
        if allies >= 0 and couleur_tour[allies] > 0:
            if couleur_tour[allies] ==1:
                points_carte += 2
            points_carte += 2
        couleur_tour[allies]  += 1
        # il faut donner des points fictifs au cartes qui ont permi d'effectuer l'achat
        redistribution(reference_achat,reference_point,ide,points_carte) # on redistribue des points aux prédescesseurs de la carte
        points[joueur] += points_carte
        reference_achat[ide][1] += points_carte # on ajoute les points gagné par la carte 
        # gestion des credits
        credits_tot += credit
        defausse[joueur].append(main[joueur][-1])
        main[joueur].pop()
    # on stoque dans les references les cartes acheté, mais aussi via quel cartes
    achat = achat_auto(credits_tot,joueur,cartes_A,defausse,magasin,ide_prospecteur)
    for k in range(len(jouer)):
        (ide,credit) = jouer[k]
        if credit > 0:
            for kp in range(len(achat)):
                reference_point[achat[kp][0]].append([ide,credit]) # on ajoute l'identifiant et la quantité de credit investi pour la carte
                reference_achat[ide].append((achat[kp][0],credit))
    pioche(5,joueur,deck,main,defausse) # on repioche a le fin du tour 5 cartes


def partie(F,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur,joueur):
    while points[0] < F and points[1] < F:
        jouer_un_tour_0(joueur,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur)
        joueur = 1 - joueur # changement de joueur
    if points[0] >= F: # un des deux joueur a remporté la partie
        return 0
    else:
        return 1

####### Une partie #######

#partie(50,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur,0)

############################# Passage à N parties ###################################

###### Les fonctions auxiliaires ######

def fill_gain(reference_achat,gain): # utilise la reference_achat pour donner le gain de chaque carte, voir dossier pour plus de renseignements
    for indice_ref in range(2,len(reference_achat)):
        [ide,cout,degats,credit,couleur,nom] = cartes[indice_ref][:6]
        carte_ref = reference_achat[indice_ref]
        if carte_ref[2] > 0: # le nbe de fois où la carte à été joué
            gain.append([ide,nom,carte_ref[1]/carte_ref[2]])# peut etre normaliser avec /a mais meh ...


def fusion(ens): # retire les doublons de l'ensemble des gains
    res = [ens[0]]
    c = 0
    for k in range(1,len(ens)):
        n1,el1,score1,nbe1 = res[c][0],res[c][1],res[c][2],res[c][3]
        n2,score2,nbe2 = ens[k][0],ens[k][2],ens[k][3]
        if n1 != n2: # nom différents
            res.append(ens[k])
            c += 1
        else:
            if nbe1 + nbe2 > 0: # la carte ( une des copies ) à déja été joué
                res[c] = [n1,el1,(score1*nbe1 + score2*nbe2)/(nbe1 + nbe2),nbe1 + nbe2]
            else: # la carte n'a jamais été joué
                res[c] = [n1,el1,0,0]
    return res


def tri_power(ens): # tri par ordre de gain, on utilise un tri rapide
    n = len(ens)
    if n <= 1 :
        return ens
    p = ens[0]
    pre = [k for k in ens[1:] if k[2]<=p[2]]
    su = [k for k in ens[1:] if k[2]>p[2]]
    return (tri_power(pre) + [p] + tri_power(su))


def algo_moyenne(longueur,F,recup_victoire):
    moyenne = []
    comparaison = [0,0]
    for i in range(2,len(cartes)):
        [ide,cout,degats,credit,couleur,nom] = cartes[i][:6]
        moyenne.append([nom,couleur,0,0]) # initialisation du gain moyen pour chaque carte
    for p in range(longueur):
        joueur = rdm.randint(0,1) # choix du premier joueur
        cartes_A = cartes[2+nbe_prospecteur:]
        reference_achat = [["scout",0,0]]
        reference_point = [[0]]
        remplir_ref(reference_achat,reference_point)
        magasin = []
        remplir_magasin(magasin,cartes_A)
        deck = [[],[]]
        remplir_deck(deck)
        defausse = [[],[]]
        main = [[],[]]
        pioche(3,joueur,deck,main,defausse)
        pioche(5,joueur,deck,main,defausse)
        points = [0,0]
        ide_prospecteur = [2]
        gagnant = partie(F,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur,joueur)
        comparaison[gagnant] += 1
        gain = []
        fill_gain(reference_achat,gain)
        for carte_jouer in gain: # calcul de moyenne pondérée
            moyenne[carte_jouer[0]-2][2] += carte_jouer[2]
            moyenne[carte_jouer[0]-2][3] += 1
    for j in range(len(moyenne)):
        if moyenne[j][3] > 0:
            moyenne[j][2] = moyenne[j][2] / moyenne[j][3]
    if not recup_victoire:
        return fusion(moyenne)
    else:
        return fusion(moyenne),comparaison

nbe1 = 1000
#final = algo_moyenne(nbe1,50,False)
#print("algo =",final)
#print("La version auto ",tri_power(final))

######################### On passe à une version plus évolué où le jeu prend en compte le gain calculé précedement ###################

#### Nouvelles fonctions ####

def ajout_puissance(final,cartes): # on ajoute au type carte la puissance
    i = 0
    cartes[0].append([0,0])
    cartes[1].append([0,0])
    for k in range(2,len(cartes)):
        [ide,a,p,c,el,n] = cartes[k][:6]
        if final[i][2] > 0:
            cartes[k].append([final[i][2],1])
        else:
            cartes[k].append([0,0]) # la carte n'a pas été joué
        if n != cartes[k-1][5] and i < len(final) -1:
            i += 1

def best_color(final):
    colors = [0,0,0,0,0]
    for c in final: # les prospecteurs ne comptent pas
        el = empreinte_couleur(c[1])
        colors[el] += c[2]/cartes_par_couleur
    return colors

A_,B_,C_ = 1,1,1
def best_move(IA,tab,magasin,tri_couleur,couleur_deck,A_,B_,C_): # tri les cartes de possible par puissance
    (place,cout,ide) = tab[0]
    couleur = empreinte_couleur(cartes[ide][4])
    A,B,C = 0,0,0
    if IA in [1,2,3,5]:
        C = C_
    if IA in [1,2,4,6]:
        B = B_
    if IA in [1,3,4,7]:
        A = A_
    if IA == -1:
        A,B,C = 57,54,74 # voir recherche de la meilleur IA
    if couleur != -1:
        best,pts = [0,ide],C * cartes[ide][6][0] + A * tri_couleur[couleur] +  B * couleur_deck[couleur]
    else:
        best,pts = [0,ide],C * cartes[ide][6][0]
    select = []
    for i in range(1,len(tab)):
        (place,cout,ide) = tab[i]
        couleur = empreinte_couleur(cartes[ide][4])
        new_pts = C * cartes[ide][6][0] + A * tri_couleur[couleur] + B * couleur_deck[couleur]
        if new_pts  > pts:
            best = [i,ide]
            pts = new_pts
        if cartes[ide][6][1] == 0:
            select.append([i,ide]) # la carte a une puissance nul du au fait qu'elle n'a encore jamais été joué
    select.append(best)
    k = rdm.randint(0,len(select)-1)
    couleur_deck[empreinte_couleur(cartes[select[k][1]][4])] += 1
    return select[k][0]

#####

#ajout_puissance(final,cartes)

###### La mise à jour des anciennes fonctions #######

# Le coeur de ce changement est la fonction achat_intelligent qui achete la carte la plus "puissante"
def achat_intelligent(c,joueur,IA,cartes_A,defausse,magasin,ide_prospecteur,tri_couleur,couleur_deck,A_,B_,C_):
    possible = []
    for i in range(len(magasin)):
        [ide,a,p,cf,e,n,sc] = magasin[i][:7]
        if a <= c:
            possible.append((i,a,ide))
    if c >= 2 and ide_prospecteur[0] < 2 + nbe_prospecteur:
        possible.append((-1,2,ide_prospecteur[0]))
    if possible == []:
        return []
    k = best_move(IA,possible,magasin,tri_couleur,couleur_deck,A_,B_,C_) # changement de buy_auto vers buy_smart avec la suppression de l'aléatoire
    (j,a_final,ide) = possible[k]
    if j >= 0:
        memoire = magasin[j]
        acheter(j,joueur,cartes_A,defausse,magasin)
        return achat_intelligent(c - a_final,joueur,IA,cartes_A,defausse,magasin,ide_prospecteur,tri_couleur,couleur_deck,A_,B_,C_) + [memoire]
    else:
        defausse[joueur].append(cartes[ide_prospecteur[0]])
        memoire = ide_prospecteur[0]
        ide_prospecteur[0] += 1
        return achat_intelligent(c - 2,joueur,IA,cartes_A,defausse,magasin,ide_prospecteur,tri_couleur,couleur_deck,A_,B_,C_) + [cartes[memoire]]


def fill_gain_evolve(reference_achat,gain): # pas de changement significatif
    for indice_ref in range(2,len(reference_achat)):
        [ide,a,p,c,e,n,sc] = cartes[indice_ref][:7] # seul changement ici avec sc
        carte_ref = reference_achat[indice_ref]
        if carte_ref[2] > 0:
            gain.append([ide,n,carte_ref[1]/carte_ref[2]])


def jouer_un_tour_evoluer(joueur,IA,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur,tri_couleur,couleur_deck,A_,B_,C_):
    credits_tot = 0
    couleur = [0,0,0,0]
    jouer = []
    while main[joueur] != []:
        [ide,a,p,c,e,n,sc] = main[joueur][-1][:7]
        jouer.append((ide,c))
        reference_achat[ide][2] += 1
        points_tour = p
        reference_achat[ide][1] += p
        allies = empreinte_couleur(e)
        if allies >= 0 and couleur[allies] > 0:
            if couleur[allies] ==1:
                reference_achat[ide][1] += 2
                points_tour += 2
            reference_achat[ide][1] += 2
            points_tour += 2
        couleur[allies] = couleur[allies] +1
        redistribution(reference_achat,reference_point,ide,points_tour)
        points[joueur] += points_tour
        credits_tot += c
        defausse[joueur].append(main[joueur][-1])
        main[joueur].pop()
    achat = achat_intelligent(credits_tot,joueur,IA,cartes_A,defausse,magasin,ide_prospecteur,tri_couleur,couleur_deck,A_,B_,C_) # on utilise achat_intelligent
    for k in range(len(jouer)):
        (ide,c) = jouer[k]
        if c > 0:
            for kp in range(len(achat)):
                reference_point[achat[kp][0]].append([ide,c])
                reference_achat[ide].append((achat[kp][0],c))
    pioche(5,joueur,deck,main,defausse)



def partie_evoluer(F,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur,tri_couleur,couleur_deck,joueur):
    while points[0] < F and points[1] < F:
        jouer_un_tour_evoluer(joueur,3,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur,tri_couleur,couleur_deck,A_,B_,C_)
        joueur = 1 - joueur
    if points[0] >= F:
        return 0
    else:
        return 1

def partie_comparaison(F,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur,tri_couleur,couleur_deck,joueur,adv,A_,B_,C_):
    while points[0] < F and points[1] < F:
        if adv[joueur] == 0:
            jouer_un_tour_0(joueur,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur)
            joueur = 1 - joueur
        else:
            jouer_un_tour_evoluer(joueur,adv[joueur],deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur,tri_couleur,couleur_deck,A_,B_,C_)
            joueur = 1 - joueur
    if points[0] >= F:
        res[0] += 1
        return 0
    else:
        res[1] += 1
        return 1

def algo_moyenne_evoluer(longueur,F,final,adv,A_,B_,C_):
    moyenne = []
    tri_couleur = best_color(final)
    comparaison = [0,0]
    for i in range(2,len(cartes)):
        [ide,a,p,c,el,n,sc] = cartes[i][:7]
        moyenne.append([n,el,0,0])
    for partie in range(longueur):
        joueur = rdm.randint(0,1)
        cartes_A = cartes[2+nbe_prospecteur:]
        reference_achat = [["scout",0,0]]
        reference_point = [[0]]
        remplir_ref(reference_achat,reference_point)
        magasin = []
        remplir_magasin(magasin,cartes_A)
        #print("Starter magasin ",magasin)
        deck = [[],[]]
        remplir_deck(deck)
        defausse = [[],[]]
        main = [[],[]]
        pioche(3,joueur,deck,main,defausse)
        pioche(5,1-joueur,deck,main,defausse)
        points = [0,0]
        ide_prospecteur = [2]
        couleur_deck = [0,0,0,0,0]
        gagnant = partie_comparaison(F,deck,main,defausse,cartes_A,points,reference_achat,reference_point,magasin,ide_prospecteur,tri_couleur,couleur_deck,joueur,adv,A_,B_,C_)
        comparaison[gagnant] += 1
        #print("reference = ",reference_achat[2:])
        gain = []
        fill_gain_evolve(reference_achat,gain)
        #print("gain =",gain)
        for carte_jouer in gain: # calcul de moyenne pondere
            #print(carte_jouer)
            moyenne[carte_jouer[0]-2][2] += carte_jouer[2]
            moyenne[carte_jouer[0]-2][3] += 1
    for j in range(len(moyenne)):
        if moyenne[j][3] > 0:
            moyenne[j][2] = moyenne[j][2] / moyenne[j][3]
    return fusion(moyenne),comparaison

#final_plus = algo_moyenne_evoluer(100,50,final)
#print()
#print("la version smart",tri_power(final_plus))

def mise_a_jour_power(final,cartes): # très similaire à ajout puissance, on met celle ci à jour
    i = 0
    for k in range(2,len(cartes)):
        [ide,a,p,c,el,n,sc] = cartes[k][:7]
        if final[i][2] > 0:
            cartes[k][6][0] = (final[i][2] + sc[0]*sc[1])/(1+sc[1]) # calcul de moyenne pondéré
            cartes[k][6][1] += 1
        if cartes[k][5] != cartes[k-1][5] and i < len(final) -1:
            i += 1

nbe2 = 1000

def meta_partie(N,nbe):
    final_plus = algo_moyenne(nbe, 50, False)
    ajout_puissance(final_plus, cartes)
    for meta in range(N-1): # on effectue toujours plus de parties !
        mise_a_jour_power(final_plus,cartes)
        final_plus,inutile = algo_moyenne_evoluer(nbe,50,final_plus,[3,3],A_,B_,C_)
        print()
        print("la version méta numéro ",meta+1,tri_power(final_plus))
        print(best_color(final_plus))

#meta_partie(3,nbe2)

def evolution(N,nbe,adv):
    final,comparaison = algo_moyenne(nbe, 75,True)
    resultat = [comparaison]
    ajout_puissance(final, cartes)
    for i in range(N-1):
        mise_a_jour_power(final,cartes)
        final,comparaison = algo_moyenne_evoluer(nbe, 75, final, adv,A_,B_,C_)
        pro = [resultat[-1][0] + comparaison[0],resultat[-1][1] + comparaison[1]]
        resultat.append(pro)
    for j in range(2):
        affiche = []
        for score in resultat:
            affiche.append(score[j])
        plt.plot(affiche)
    plt.legend(adv)
    plt.show()

def evolution_recherche(N,nbe,A_,B_,C_):
    final,comparaison = algo_moyenne(nbe, 75,True)
    resultat = comparaison
    ajout_puissance(final, cartes)
    for i in range(N-1):
        mise_a_jour_power(final,cartes)
        final,comparaison = algo_moyenne_evoluer(nbe, 75, final, [1,-1],A_,B_,C_)
        resultat = [resultat[0] + comparaison[0],resultat[1] + comparaison[1]]
    return resultat[0] > resultat[1]

evolution(100,100,[4,1,3])
# print(res)

def recherche_IA():
    a_,b_,c_,ecart = 50,50,50,10
    while not evolution_recherche(100,100,a_,b_,c_):
        b_ = rdm.random()*2*ecart + (b_-ecart)
        a_ = rdm.random()*2*ecart + (a_-ecart)
        c_ = rdm.random()*2*ecart + (c_-ecart)
    return a_,b_,c_

# print(recherche_IA())
